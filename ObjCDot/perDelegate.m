//
//  perDelegate.m
//  ObjCDot
//
//  Created by Grant Empey on 11/21/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import "perDelegate.h"
#import "ViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

@implementation perDelegate
- (void)peripheral:(CBPeripheral *)peripheral
didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error {
    if (error) {
        NSLog(@"Error changing notification state: %@",
              [error localizedDescription]);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverServices:(NSError *)error {
    for (CBService *service in peripheral.services) {
        if ([service.UUID.UUIDString isEqualToString:@"6E400001-B5A3-F393-E0A9-E50E24DCCA9E"]) {
            _service = service;
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
}


- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(CBService *)service
             error:(NSError *)error {
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID.UUIDString containsString:@"6E400002"]) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            _sendCharacteristic = characteristic;
            NSData* c = [@"c1" dataUsingEncoding:NSUTF8StringEncoding];
            [self sendMessage:c];
        } else if ([characteristic.UUID.UUIDString containsString:@"6E400003"]) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            _receiveCharacteristic = characteristic;
        }
        
    }
}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error {
    NSString *currentVal = [[NSString alloc] initWithData:_receiveCharacteristic.value encoding:NSUTF8StringEncoding];
    NSString *printVal = [ currentVal stringByAppendingString:@"\n"];
    NSLog(@"%@", currentVal);
    if ([currentVal isEqualToString:@"BUTT PRESS"]) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate spotifyLinkup];
    }
    _actionLog.text = [printVal stringByAppendingString:_actionLog.text];
    NSLog(@"%@", [error localizedDescription]);
}

@end
