//
//  ViewController.h
//  ObjCDot
//
//  Created by Grant Empey on 9/29/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>


@interface ViewController : UIViewController<CBCentralManagerDelegate, CBPeripheralDelegate, UITextFieldDelegate, UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *backGroundWeb;
@property (strong, nonatomic) CBCentralManager *centralManager;
@property (strong, nonatomic) CBPeripheral *discoveredPeripheral;
@property (strong, nonatomic) NSMutableData *data;
@property (strong, nonatomic) CBCharacteristic *sendCharacteristic;
@property (strong, nonatomic) CBCharacteristic *receiveCharacteristic;
@property (strong, nonatomic) CBService *service;
@property (strong, nonatomic) IBOutlet UITextField *inputBox;
@property (strong, nonatomic) IBOutlet UITextView *actionLog;

@end

