//
//  perDelegate.h
//  ObjCDot
//
//  Created by Grant Empey on 11/21/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol perDelegate <NSObject>
@required
- (void)saveContext;
@optional

@end

@interface perDelegate : NSObject <perDelegate>

@end
