//
//  ViewController.m
//  ObjCDot
//
//  Created by Grant Empey on 9/29/15.
//  Copyright © 2015 ExampleCompany. All rights reserved.
//

#import "ViewController.h"
#import <Spotify/Spotify.h>
#import "AppDelegate.h"
@interface ViewController ()
@property (nonatomic, strong) SPTSession *session;
@property (nonatomic, strong) SPTAudioStreamingController *player;

@end

@implementation ViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _inputBox.delegate = self;
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{ CBCentralManagerOptionRestoreIdentifierKey: @"myCentralManagerIdentifier" }];
    _data = [[NSMutableData alloc] init];
    [self scan];
    
}


- (void)scan {
    switch ([_centralManager state])
    {
        case CBCentralManagerStateUnsupported:
            NSLog(@"The platform/hardware doesn't support Bluetooth Low Energy.");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"The app is not authorized to use Bluetooth Low Energy.");
            break;
        case CBCentralManagerStatePoweredOff:
            NSLog(@"Bluetooth is currently powered off.");
            break;
        case CBCentralManagerStatePoweredOn:
            NSLog(@"powered On");
            [_centralManager scanForPeripheralsWithServices:nil options:nil];
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"update");
            break;
        default:
            NSLog(@"everything is broken");
            
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    [self scan];
}


- (void)centralManager:(CBCentralManager *)central
     didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI {
    
    
    if ([peripheral.name isEqualToString:@"DOT"]) {
        //[self.centralManager stopScan];
        _discoveredPeripheral = peripheral;
        peripheral.delegate = self;
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
}

-(void)spotifyLinkup {
    [[SPTAuth defaultInstance] setClientID:@"6250f59c04d24abbace2e0daaa239bbf"];
    [[SPTAuth defaultInstance] setRedirectURL:[NSURL URLWithString:@"ObjCDot://callback"]];
    [[SPTAuth defaultInstance] setRequestedScopes:@[SPTAuthStreamingScope]];
    
    // Construct a login URL and open it
    NSURL *loginURL = [[SPTAuth defaultInstance] loginURL];
    
    // Opening a URL in Safari close to application launch may trigger
    // an iOS bug, so we wait a bit before doing so.
    [[UIApplication sharedApplication] performSelector:@selector(openURL:)
                      withObject:loginURL afterDelay:0.1];

}
- (void)centralManager:(CBCentralManager *)central
  didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%@", peripheral.name);
    [peripheral discoverServices:nil];
}


- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverServices:(NSError *)error {
    for (CBService *service in peripheral.services) {
        if ([service.UUID.UUIDString isEqualToString:@"6E400001-B5A3-F393-E0A9-E50E24DCCA9E"]) {
            _service = service;
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
}


- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(CBService *)service
             error:(NSError *)error {
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([characteristic.UUID.UUIDString containsString:@"6E400002"]) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            _sendCharacteristic = characteristic;
            NSData* c = [@"c1" dataUsingEncoding:NSUTF8StringEncoding];
            [self sendMessage:c];
        } else if ([characteristic.UUID.UUIDString containsString:@"6E400003"]) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            _receiveCharacteristic = characteristic;
        }
        
    }
}


- (IBAction)sendInfo:(id)sender {
    [_inputBox resignFirstResponder];
    NSLog(@"%@", _inputBox.text);
}


- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(nonnull CBCharacteristic *)characteristic error:(nullable NSError *)error {
    NSString *currentVal = [[NSString alloc] initWithData:_receiveCharacteristic.value encoding:NSUTF8StringEncoding];
    NSString *printVal = [ currentVal stringByAppendingString:@"\n"];
    NSLog(@"%@", currentVal);
    if ([currentVal isEqualToString:@"BUTT PRESS"]) {
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate spotifyLinkup];
    }
    _actionLog.text = [printVal stringByAppendingString:_actionLog.text];
    NSLog(@"%@", [error localizedDescription]);
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"%@", _inputBox.text);
    NSData* c = [_inputBox.text dataUsingEncoding:NSUTF8StringEncoding];
    _inputBox.text = @"";
    [_inputBox resignFirstResponder];
    [self sendMessage:c];
    
    return YES;
}


- (void) sendMessage:(NSData *)toSend {
    if (_sendCharacteristic != nil) {
        [self.discoveredPeripheral writeValue:toSend forCharacteristic:_sendCharacteristic type:CBCharacteristicWriteWithoutResponse];
    } else {
        NSLog(@"sender is null broh");
    }
}


- (void)runScript {
    _backGroundWeb = [[UIWebView alloc] init];
    _backGroundWeb.delegate = self;
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[[NSBundle mainBundle] URLForResource:@"sample" withExtension:@"html"]];
    [_backGroundWeb loadRequest:urlRequest];
    NSLog(@"view did done");
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"%@", [webView stringByEvaluatingJavaScriptFromString:@"remoteCall();"]);
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"%@", [request.URL scheme]);
    
    switch (navigationType) {
            
        case UIWebViewNavigationTypeOther:
            if ([[request.URL scheme] isEqualToString:@"alert"]) {
                NSString *message = [request.URL host];
                NSString *dataString = [webView stringByEvaluatingJavaScriptFromString:@"getData();"];
                _actionLog.text = [dataString stringByAppendingString:_actionLog.text];

                
                if (true) {
                    // the alert should be shown
                } else {
                    // don't show the alert
                    // just do nothing
                }
                
                return NO;
            }
            break;
        default: ;
            //ignore
    }
    return YES;
}


- (void)peripheral:(CBPeripheral *)peripheral
didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error {
    if (error) {
        NSLog(@"Error changing notification state: %@",
              [error localizedDescription]);
    }
}


- (void)centralManager:(CBCentralManager *)central
didDisconnectPeripheral:(CBPeripheral *)peripheral
                 error:(NSError *)error {
    [central connectPeripheral:peripheral options:nil];

}

- (void)centralManager:(CBCentralManager *)central
      willRestoreState:(NSDictionary<NSString *,
                        id> *)dict {
    _actionLog.text = [@"I HAVE RETURNED" stringByAppendingString:_actionLog.text];
}



@end
